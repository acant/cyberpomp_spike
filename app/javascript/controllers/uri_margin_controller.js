import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ "template", "tbody", "key" ]

  connect() {
    console.log("uri margins connect");
  }

  delete(event) {
    event.target.closest("tr").remove();
  }

  add(event) {
    // TODO: Add a check that all the key inputs have values.
    console.log(this.keyTargets);

    const rowClone = this.templateTarget.content.cloneNode(true);
    const documentFragment = this.tbodyTarget.appendChild(rowClone);

    this.keyTargets[this.keyTargets.length - 1].focus();
  }
}
