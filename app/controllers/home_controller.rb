class HomeController < ApplicationController
  def index
    @aspects = []
    @years   = []

    Pathname.new('/home/andrew/aspects/').children.each do |aspect_pathname|
      @aspects.push(aspect_pathname.basename)
      @years.push((aspect_pathname + Pathname.new('dated')).children(false))
    end

    @years = @years.flatten.uniq.sort
  end
end
