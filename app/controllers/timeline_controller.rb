class TimelineController < ApplicationController
  def show_year
    @year = params[:year].to_i
  end

  def show_week
    @year = params[:year].to_i
    @week = params[:week].to_i
  end

  def show_month
    @year  = params[:year].to_i
    @month = params[:month].to_i
  end

  def show_day
    @year      = params[:year].to_i
    @month     = params[:month].to_i
    @day       = params[:day].to_i
    @date_time = DateTime.new(@year, @month, @day)

    log_pathname =
      Pathname
      .new("/home/andrew/aspects/prime/dated/#{@year}/log")
      .join(@month.to_s.rjust(2, '0'), @day.to_s.rjust(2, '0'))

    @journal = ReadSourceService.new(log_pathname.join('journal'))
    @inbox   = ReadSourceService.new(log_pathname.join('inbox'))
  end
end
