require 'csv'

class ListsController < ApplicationController
  def index_books_read
    @books = []

    year_pathnames
      .map { |x| x.join('misc', 'books_read.csv') }
      .select { |x| x.exist? }
      .map do |x|
        csv = CSV.table(x, headers: true, col_sep: ',').to_a
        csv.delete_at(0)
        @books.concat(csv.reverse)
      end
  end

  def index_books_to_read
    @books = []

    year_pathnames
      .map { |x| x.join('misc', 'books_to_read.csv') }
      .select { |x| x.exist? }
      .map do |x|
        csv = CSV.table(x, headers: true, col_sep: ',').to_a
        csv.delete_at(0)
        @books.concat(csv.reverse)
      end
  end

  ##############################################################################

  # @return [Hash{Symbol=>Hash{Integer=>Pathnames}}]
  def year_pathnames
    @year_pathnames = []

    Pathname.new('/home/andrew/aspects/prime/dated').children.each do |year_pathname|
      @year_pathnames.push(year_pathname)
    end

    @year_pathnames.sort!.reverse!
  end
end
