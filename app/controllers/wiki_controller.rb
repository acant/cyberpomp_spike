class WikiController < ApplicationController
  def index
    wiki_pages

    @category_wiki_pages = {}
    wiki_pages.each do |key, data|
      [data[:frontmatter]['category'], data[:frontmatter]['catgories']].flatten.compact.each do |category|
        @category_wiki_pages[category] ||= {}
      end
    end
  end

  def show
    @key       = params[:id]
    @title     = @key
    @wiki_page = wiki_pages[@key]
    @pathnames = @wiki_page[:pathnames]

    @index = ReadSourceService.new(@pathnames.first)
    @title = @index.frontmatter['title'] || @key
  end

  def edit
  end

  def update
  end

  ##############################################################################

  private

  def repo_pathnames
    return @repo_pathnames if @repo_pathnames

    @repo_pathnames =
      Pathname.new('/home/andrew/aspects/prime/dated/')
      .children
      .sort.reverse.map { |x| x.join('wiki') }.select(&:exist?)
    @repo_pathnames.push(Pathname.new('/home/andrew/aspects/prime/wiki/'))

    @repo_pathnames
  end


  def wiki_pages
    return @wiki_pages if @wiki_pages

    @wiki_pages = {}
    repo_pathnames.each do |repo_pathname|
      repo_pathname
        .children
        .select(&:file?)
        .reject { |x| x.basename.to_s.start_with?('.') }
        .each do |pathname|
          key = pathname.basename('.*').to_s
          @wiki_pages[key]              ||= {}
          @wiki_pages[key][:pathnames]  ||= []
          @wiki_pages[key][:categories] ||= []
          @wiki_pages[key][:tags]       ||= []
          @wiki_pages[key][:pathnames].push(pathname)
        end

      @wiki_pages.each_key do |key|
        pp key
        @wiki_pages[key][:frontmatter] =
          ReadSourceService.new(@wiki_pages[key][:pathnames].first).frontmatter
      end
    end

    @wiki_pages
  end
end
