require 'csv'

class URIController < ApplicationController
  def index
    if params[:uri]
      redirect_to(uri_path(params[:uri]))
      return
    end

    # TODO try looking up the date based upon the sha
    @urls = URIDataService.new.search
  end

  def show
    @title      = params[:title]
    @href       = uri
    @tags       = []
    @tag_source = ''

    if tag_pathname&.exist?
      @tag_year   = tag_pathname.to_s.match(/dated\/([0-9]{4})\//)[1]

      @tag_source = tag_pathname.read
      tags_table = CSV.table(tag_pathname, col_sep: "\t").to_a
      @href  = Addressable::URI.parse(tags_table.find { |x| x.first == 'href' }.last)
      @title = tags_table.find { |x| x.first == 'title' }&.last
      @tags  = tags_table.reject { |x| x.first == :tag || x.first == 'href' || x.first == 'title' }

      # TODO: Figure how the right way to show this?
      # Maybe move into a seperate object
      # Which will then wrap around the repo AND database.
      uri_margins_pathname = Pathname.new("/home/andrew/aspects/prime/dated/#{@tag_year}/uri_margins")
      @commits = Git.open(uri_margins_pathname.to_s).log.path(@href.to_pathname).all
    end

    @index = ReadSourceService.new(index_markdown_pathname)

    @open_library =
      if @href.isbn?
        begin
          OpenLibraryBook.new(@href.isbn)
        rescue
        end
      end
  end

  def edit
    @href  = uri
    @index = ReadSourceService.new(index_markdown_pathname)
  end

  def update
    to_tsv = CSV.generate(col_sep: "\t") do |csv|
      csv << %w[Tag Value]
      csv << [:href, uri]
      csv << [:title, params[:title]] if params[:title].present?
      params[:key].each_index do |i|
        key   = params[:key][i]
        value = params[:value][i]

        if key.present?
          if value.present?
            csv << [key, value]
          else
            csv << [key]
          end
        end
      end
    end

    git = Git.open(repo_pathname)

    # Write tags
    writeable_tag_pathname = tag_pathnames.first

    writeable_tag_pathname.dirname.mkpath
    writeable_tag_pathname.write(to_tsv)

    begin
      git.add(writeable_tag_pathname)
      git.commit_all("Tag #{uri}")
    rescue Git::GitExecuteError
      # Nothing to do, continue with the rest of the sync.
    end

    # Write index
    writeable_index_markdown_pathname = index_markdown_pathnames.first
    if params[:index_markdown].present?
      writeable_index_markdown_pathname.write(
        params[:index_markdown] + "\n"
      )
    else
      writeable_index_markdown_pathname.delete if writeable_index_markdown_pathname.exist?
    end

    begin
      git.add(writeable_index_markdown_pathname)
      git.commit_all("Notes for #{uri}")
    rescue Git::GitExecuteError
      # Nothing to do, continue with the rest of the sync.
    end

    # Sync the commits
    begin
      git.pull('origin')
      git.push('origin')
    rescue Git::GitExecuteError
    end

    # TODO: actually should just re-render the page
    redirect_to(uri_path(uri))
  end

################################################################################

  private

  def index_repo_pathnames
    @index_repo_pathnames ||= repo_pathnames[0, 2]
  end

  def repo_pathnames
    @repo_pathnames ||=
      Pathname.new('/home/andrew/aspects/prime/dated/')
      .children
      .sort.reverse.map { |x| x.join('uri_margins') }.select(&:exist?)
  end

  def repo_pathname
    @repo_pathname ||=
      Pathname.new("/home/andrew/aspects/prime/dated/#{Time.current.year}/uri_margins")
  end

  def uri
    @uri ||= Addressable::URI.parse(params[:id].sub(%r{:/+([^/])}, '://\1'))
  end

  def uri_pathnames
    @uri_pathnames ||= repo_pathnames.map { |x| x + uri.to_pathname }
  end

  def tag_pathnames
    @tag_pathnames ||= uri_pathnames.map { |x| x + 'tags.tsv' }
  end

  def index_markdown_pathnames
    @index_markdown_pathnames ||= uri_pathnames.map { |x| x + 'index.markdown' }
  end

  def tag_pathname
    @tag_pathname ||= tag_pathnames.find { |x| x.exist? }
  end

  def index_markdown_pathname
    @index_markdown_pathname ||=
      index_markdown_pathnames.find { |x| x.exist? }
  end
end
