require 'csv'
require 'git'

class URIDataService
  def search(q: nil, year:nil)
    results = {}

    search_repo_pathnames =
      if year
        [
          Pathname.new("/home/andrew/aspects/prime/dated/#{year}/uri_margins")
        ]
      else
        index_repo_pathnames
      end

    search_repo_pathnames.each do |repo_pathname|
      git  = Git.open(repo_pathname)
      year = repo_pathname.to_s.match(/dated\/([0-9]{4})\//)[1]

      git.ls_files.keys
      .map { |x| Pathname.new(x) }
      .select { |x| x.basename == Pathname.new('tags.tsv') }
      .each do |x|
        pathname   = repo_pathname + x
        tags_table = CSV.table(pathname, col_sep: "\t").to_a
        href       = tags_table.find { |x| x.first == 'href' }.last
        date       = git.log(1).path(x).all.first.author_date
        results[href] =
          {
            pathname: pathname,
            date:     date,
            href:     href
          }
      end
    end

    results
  end

  def find(uri_input)
    uri = Addressable:uri.parse(uri_input)

    # TODO: All the search handle looking to https or http
    # Might actually want to find all of the entires, to show history.
    # But the first one would be the current values.

    #tag_pathnames = uri_pathnames.map { |x| x + 'tags.tsv' }
  end

  ##############################################################################

  def repo_pathnames
    @repo_pathnames ||=
      Pathname.new('/home/andrew/aspects/prime/dated/')
      .children
      .sort.reverse.map { |x| x.join('uri_margins') }.select(&:exist?)
  end

  def index_repo_pathnames
    @index_repo_pathnames ||= repo_pathnames[0, 2]
  end
end
