require 'kramdown'
require 'kramdown-parser-gfm'
require 'rinku'
require 'yaml'
require 'wikicloth'

# = Frontmatter =
# * https://github.com/bridgetownrb/bridgetown/blob/main/bridgetown-core/lib/bridgetown-core/front_matter/loaders/yaml.rb#L33
# * https://github.com/middleman/middleman/blob/d4407fd283cd5abe0a01cfbddd5e3fe786ea9f6c/middleman-core/lib/middleman-core/util/data.rb#L50
# * https://github.com/calebhearth/frontmatter
#
# = Formats =
# * https://en.wikipedia.org/wiki/Help:Wikitext
class ReadSourceService
  # @param pathname [nil, Pathname]
  def initialize(pathname)
    @pathname = pathname
  end

  attr_reader :pathname

  # @return [nil]
  def content_type
    return unless @pathname
    case @pathname.extname
    when '.markdown', '.mkd', '.md'   then 'text/markdown'
    when '.mediawiki', '.wiki', '.mw' then 'text/x-mediawiki'
    else
      'text'
    end
  end

  # @return [nil]
  # @return [String]
  def content
    return unless @pathname&.exist?

    @content ||= @pathname&.read
  end

  HEADER = %r!\A---[ \t]*\n!
  BLOCK = %r!#{HEADER.source}(.*?\n?)^((---|\.\.\.)[ \t]*$\n?)!m

  # @return [nil]
  # @return [Regexp]
  def frontmatter_matcher
    return unless content

    @frontmatter_matcher ||=
      begin
        content.match(BLOCK)
      rescue ArgumentError
      end
  end

  # @return [Hash]
  def frontmatter
    return {} unless frontmatter_matcher

    @frontmatter ||=
      YAML.load(
        frontmatter_matcher[1],
        permitted_classes: [Date]
      )
  end

  # @return [nil]
  # @return [String]
  def source
    return content unless frontmatter_matcher

    @source ||= frontmatter_matcher.post_match.lstrip
  end

  # @return [nil]
  # @return [String]
  # @return [ActiveSupport::SafeBuffer]
  def html
    return unless source

    @html ||=
      case content_type
      when 'text/markdown'
        Rinku.auto_link(
          Kramdown::Document.new(source, input: 'GFM').to_html
        ).html_safe
      when 'text/x-mediawiki'
        WikiCloth::Parser.new(data: source).to_html.html_safe
      else
        "<pre>#{Rinku.auto_link(source)}</pre>".html_safe
      end
  end
end
