module ApplicationHelper
  def uri_link_to(uri_value, title = nil)
    uri = Addressable::URI.parse(uri_value)

    if uri.urn?
      fail unless uri.isbn?

      link_to(
        title.present? ? title : uri.path,
        uri_path(uri.to_s)
      )
    else
      link_to(
        title.present? ? title : uri_value,
        uri_value,
        class: 'external'
      ) + content_tag(
        :div, link_to('URI page', uri_path(uri.to_s))
      )
    end
  rescue
    if title.present?
      "#{title}(#{uri_value})"
    else
      uri_value
    end
  end
end
