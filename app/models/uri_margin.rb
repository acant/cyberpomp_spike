class URIMargin < ApplicationRecord
  has_many :tags, class_name: 'URIMarginTag'
end
