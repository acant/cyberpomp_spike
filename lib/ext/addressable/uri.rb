require 'addressable'
require 'digest'

# Patch for Addresable::URI add the following methods:
# * to_pathname
# * openlibrary?
# * wikipedia?
#
# @see https://github.com/sporkmonger/addressable/blob/main/lib/addressable/uri.rb
module Addressable
  class URI
    # @return [Pathname]
    def to_pathname
      pathname = Pathname.new(scheme)
      pathname += trunc_sha1(host) if host
      pathname += trunc_sha1(port.to_s) if port

      path_parts =
        if scheme == 'urn'
          path.split(':')
        else
          path.sub(/\//,'').split('/')
        end
      path_parts.map { |x| pathname += trunc_sha1(x) }

      pathname += "?#{trunc_sha1(query)}"    if query
      pathname += "##{trunc_sha1(fragment)}" if fragment

      pathname
    end

    # @return [Boolean]
    def urn?
      scheme == 'urn'
    end

    # @return [Boolean]
    def isbn?
      return false unless urn?

      path =~ /isbn:.+/
    end

    # @return [nil]
    # @return [String]
    def isbn
      return unless isbn?

      path.sub('isbn:', '')
    end

    ############################################################################

    private

    # @param input [String]
    #
    # @return [String]
    def trunc_sha1(input)
      return input if input.length <= 255

      Digest::SHA1.hexdigest(input)
    end
  end
end
