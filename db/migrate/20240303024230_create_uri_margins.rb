class CreateURIMargins < ActiveRecord::Migration[7.0]
  def change
    create_table :uri_margins do |t|
      t.timestamps
      t.string :href
      t.string :filename
      t.string :committed
      t.string :body
      t.string :body_format
      t.string :aspect
      t.integer :year
    end

    create_table :uri_margin_tags do |t|
      t.timestamps
      t.belongs_to :uri_margin
      t.string :key
      t.string :value
    end
  end
end
