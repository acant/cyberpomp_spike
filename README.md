# Cyberpomp

This is a Web UI for viewing and editing data stored in aspect directories and
syncronized with [conjoiner](https://gitlab.com/acant/conjoiner). It will also
integrate with other external data source.

This README would normally document whatever steps are necessary to get the
application up and running.

# Installation

```
sudo apt install rbenv ruby-build libyaml-dev libidn11-dev
rbenv install
# Make sure that rbenv is setup to select the ruby version using .ruby-version
bundle install
rake db:migrate
bin/dev
# Maybe restart your shell if foreman was installed
bin/dev
```

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

```
sudo apt install rbenv ruby-build libyaml-dev
rbenv install
# Make sure that rbenv is setup to select the ruby version using .ruby-version
bundle install
```



* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

# Data design

## Frontmatter
- `title`
- `dates`
  * lsit of dates which are related to the article
- `status: published`
- `syndication`
  * URL(s) which the article or wiki are published to
  * https://indieweb.org/POSSE
- `upstream`
  * other Wikis to consider updating with 
- `tags`
- `categories`


# Similar Projects

## Bookmarks and Annotations
* https://github.com/quambene/bogrep
* https://github.com/ckolderup/postmarks
* https://github.com/sissbruecker/linkding
* https://pinboard.in/
* https://github.com/linkwarden/linkwarden
* https://github.com/beromir/Servas
* https://github.com/goniszewski/grimoire
* https://github.com/hoarder-app/hoarder
* https://github.com/Kovah/LinkAce/
* https://readeck.org/en/
* https://github.com/shaarli/Shaarli
* https://github.com/go-shiori/shiori
* https://raindrop.io/

### Sources
* https://news.ycombinator.com/item?id=41533958

## Aggregators
* https://github.com/fossar/selfoss
* [Bloomberg Terminal](https://en.wikipedia.org/wiki/Bloomberg_Terminal)
* https://huly.io/

## Contact Management
* https://www.ntwrk.site/

## Note taking

[[https://en.wikipedia.org/wiki/Comparison_of_note-taking_software]] on Wikipedia.

* [Obsidian](https://obsidian.md/)
* [Logseq](https://logseq.com/)
* [Microsoft OneNote](https://products.office.com/onenote) [wikipedia](https://en.wikipedia.org/wiki/Microsoft_OneNote)
* [Evernote](https://evernote.com/) [wikipedia](https://en.wikipedia.org/wiki/Evernote)
* [Meogic Wiki](https://doc.meogic.com/)
* [Zettlr](https://www.zettlr.com/download) [wikipedia](https://en.wikipedia.org/wiki/Zettlr)
  - this one also claim to help guide the writing/research process
* [TiddlyWiki](https://tiddlywiki.com/) [wikipedia](https://en.wikipedia.org/wiki/TiddlyWiki)
* [Wiki.js](https://js.wiki/)
* [Are.na](https://www.are.na/)
  - this one is more public and social than some of the others

## Book and Reading Management
* Goodreads
* OpenLibrary
* https://creativerly.com/there-is-still-the-need-for-a-better-goodreads-alternative/
* https://omnivore.app/about

## Personal Data Systems
* https://wiki.secretgeek.net/personal-data-liberation
* https://www.joshcanhelp.com/personal-data-pipeline/
* https://dogsheep.github.io/
  - https://simonwillison.net/2020/Nov/14/personal-data-warehouses/
* https://beepb00p.xyz/myinfra.html

# Research

* https://joshleeb.com/posts/linking-bookmarks.html

## URLs and URIs
* https://ittavern.com/url-explained-the-fundamentals/
* https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
