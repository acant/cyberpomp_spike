Rails.application.routes.draw do
  resources :uri, id: /.*/,
    format:   false,
    defaults: { format: 'html' },
    only:     %i[index show update edit]

  get 'lists/books_read',    to: 'lists#index_books_read'
  get 'lists/books_to_read', to: 'lists#index_books_to_read'

  get ':year',        to: 'timeline#show_year', constraints: { year: /\d{4}/ }
  get ':year/W:week', to: 'timeline#show_week', constraints: { year: /\d{4}/, week: /\d{1,2}/ }
  get ':year/:month', to: 'timeline#show_month', constraints: { year: /\d{4}/, month: /\d{1,22}/ }
  get ':year/:month/:day', to: 'timeline#show_day', constraints: { year: /\d{4}/, month: /\d{1,2}/, day: /\d{1,2}/ }

#  get ':year', to: 'timeline#index'

  # resources :tags
  resources :wiki, id: /.*/
  # resources :$year/misc/books_read
  # resources :$year/

#  get 'uri_margins/:id', to: 'uri_margins#show', id: /.*/

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root 'home#index'
end
